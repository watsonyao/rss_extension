function awesome() {
  // Do something awesome!
  console.log('awesome')
}

function filterResp(data) {
  var result = []
  for (var i = 0; i < data.length; i++) {
    var item = {}
    item.title = data[ i ].title
    item.url = data[ i ].link
    result.push(item)
  }
  return result
}

function totallyAwesome() {
  // do something TOTALLY awesome!

}

function getNews(source) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "http://123.206.116.112:3013/api/news?source=" + source, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      // JSON.parse does not evaluate the attacker's scripts.
      var resp = JSON.parse(xhr.responseText);
      var filter = filterResp(resp)
      console.log('resp', filter)
      for (var i = 0; i < filter.length; i++) {
        var value = '<li class="collection-item"><a href="' + filter[ i ].url + '" target="_blank">' + filter[ i ].title + '</a></li>'
        if (source === 'xituandroid') {
          $("#resp_1").append(value);
        } else if (source === 'v2ex') {
          $("#resp_2").append(value);
        } else if (source === 'sf') {
          $("#resp_3").append(value);
        } else if (source === 'infoq') {
          $("#resp_4").append(value);
        } else if (source === 'cnbeta') {
          $("#resp_5").append(value);
        } else if (source === 'zhihu') {
          $("#resp_6").append(value);
        } else if (source === 'pmcaff') {
          $("#resp_7").append(value);
        } else if (source === 'wqrb') {
          $("#resp_8").append(value);
        }
      }
    }
  }
  xhr.send();
}

function handleStateChange() {

}

function awesomeTask() {
  awesome();
  totallyAwesome();
}

function clickHandler(e) {
  setTimeout(awesomeTask, 1000);
}

function main() {
  // Initialization work goes here.
  console.log('main run')
}

// Add event listeners once the DOM has fully loaded by listening for the
// `DOMContentLoaded` event on the document, and adding your listeners to
// specific elements when it triggers.
document.addEventListener('DOMContentLoaded', function () {
  // document.querySelector('button').addEventListener('click', clickHandler);
  getNews("xituandroid");
  getNews("v2ex");
  getNews("sf");
  getNews("infoq");
  getNews("cnbeta");
  getNews("zhihu");
  getNews("pmcaff");
  getNews("wqrb");
  // getNews("engadget");
  // getNews("verge");
  // getNews("randroiddev");
  // getNews("36kr");
});

console.log('mypage js run')